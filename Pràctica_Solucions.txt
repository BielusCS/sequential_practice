PRÀCTICA TÈCNICA - SOLUCIONS


>> Bloc GUI (Graphical User Interface) <<

a) Directoris i Fitxers

    1. Crear el directori "GUI_Block"

        - Prémer el botó dret del mouse.
        - Clicar l'ítem "Nuevo".
        - Clicar l'ítem "Carpeta".
        - Escriure "GUI_Block" com a nom del directori.

    2. Crear els subdirectoris "GUI_Block\First", "GUI_Block\Documents" i "GUI_Block\Screenshots"

        - Realitzar doble clic al directori "GUI_Block".
        - Crear el subdirectori "First".
        - Crear el subdirectori "Documents".
        - Crear el subdirectori "Screenshots".

    3. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_A.PNG".

    4. Renombrar el subdirectori "GUI_Block\First" --> "Second"

        - Realitzar doble clic lent sobre el subdirectori "First" (selecció + edició de nom).
        - Escriure "Second" com a nou nom del directori.

    5. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_B.PNG".

    6. Eliminar el subdirectori "GUI_Block\Second"

        - Realitzar un clic sobre el subdirectori "Second" (selecció).
        - Prémer la tecla "Supr" del teclat.

    7. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_C.PNG".

    8. Crear l'accés directe "GUI_Block\docs" al subdirectori "GUI_Block\Documents"

        - Prémer el botó dret del mouse sobre el subdirectori "Documents".
        - Clicar l'ítem "Crear acceso directo".
        - Realitzar un clic sobre el nou accés directe (edició de nom).
        - Escriure "docs" com a nou nom de l'accés directe.

    9. Crear l'accés directe "GUI_Block\scrs" al subdirectori "GUI_Block\Screenshots"

        - Prémer el botó dret del mouse sobre el subdirectori "Screenshots".
        - Clicar l'ítem "Crear acceso directo".
        - Realitzar un clic sobre el nou accés directe (edició de nom).
        - Escriure "scrs" com a nou nom de l'accés directe.

    10. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_D.PNG".

    11. Moure les captures de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Seleccionar les 4 screenshots amb el mouse.
        - Amb el mouse, arrossegar els 4 fitxers fins al subdirectori "Screenshots".

    12. Guardar una captura de pantalla al directori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_E.PNG".

    13. Crear el fitxer "GUI_Block\Hello.txt"

        - Prémer el botó dret del mouse.
        - Clicar l'ítem "Nuevo".
        - Clicar l'ítem "Documento de texto".
        - Escriure "Hello" com a nom del fitxer.

    14. Inserir el contingut "Hello World" al fitxer "GUI_Block\Hello.txt"

        - Realitzar doble clic sobre el fitxer "Hello.txt".
        - Inserir el text "Hello World" usant l'editor de text.
        - Guardar els canvis.

    15. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_F.PNG".

    16. Renombrar el fitxer "GUI_Block\Hello.txt" --> "HelloWorld.txt"

        - Realitzar doble clic lent sobre el fitxer "Hello.txt" (selecció + edició de nom).
        - Escriure "HelloWorld" com a nou nom del fitxer.

    17. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_G.PNG".

    18. Canviar l'extensió del fitxer "GUI_Block\HelloWorld.txt" --> "HelloWorld.c"

        - Realitzar doble clic lent sobre el fitxer "HelloWorld.txt" (selecció + edició de nom).
        - Substituïr l'extensió ".txt" per ".c".

    19. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_H.PNG".

    20. Crear una còpia del fitxer "GUI_Block\HelloWorld.c" --> "GUI_Block\Documents\HelloWorld_orig.c"

        - Prémer el botó dret del mouse sobre el fitxer "HelloWorld.c".
        - Clicar l'ítem "Copiar".
        - Realitzar doble clic sobre el directori "Documents".
        - Prémer el botó dret del mouse.
        - Clicar l'ítem "Pegar".
        - Realitzar un clic sobre el fitxer "HelloWorld.c".
        - Escriure "HelloWorld_orig" com a nou nom del fitxer.

    21. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_I.PNG".

    22. Eliminar el fitxer "GUI_Block\HelloWorld.c"

        - Realitzar un clic sobre el fitxer "HelloWorld.c" (selecció).
        - Prémer la tecla "Supr" del teclat.

    23. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_J.PNG".

    24. Crear l'accés directe "GUI_Block\Hola" al fitxer "GUI_Block\Documents\HelloWorld_orig.c"

        - Prémer el botó dret del mouse sobre el fitxer "HelloWorld_orig.c".
        - Clicar l'ítem "Crear acceso directo".
        - Realitzar un clic sobre el nou accés directe (edició de nom).
        - Escriure "Hola" com a nou nom de l'accés directe.
        - Prémer la combinació de tecles "Ctrl + X".
        - Navegar fins al directori superior "GUI_Block".
        - Prémer la combinació de tecles "Ctrl + V".

    25. Guardar una captura de pantalla al directori "GUI_Block\"

        - Usant l'eina "Recortes", guardar el fitxer "GUI_K.PNG".

    26. Moure les captures de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Seleccionar les 6 screenshots amb el mouse.
        - Amb el mouse, arrossegar els 6 fitxers fins al subdirectori "Screenshots".

b) Navegador Web

    1. Obrir el navegador

        - Es pot realitzar un doble clic sobre la icona "Google Chrome" de l'escriptori.
        - O també es poden seguir aquests passos:

            > Clicar la icona "Windows" de la barra de tasques
            > Escriure 'ch' al camp de cerca
            > Clicar l'ítem "Google Chrome" al llistat de resultats

    2. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Browser_A.PNG".

    3. Carregar la web de Google

        - Des de la "search bar", cercar l'adreça "www.google.com".

    4. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Browser_B.PNG".

    5. Obrir una nova pestaña

        - Es pot prémer la combinació de tecles "Ctrl + T".
        - O també es poden seguir aquests passos:

            > Prémer la icona "Tres Punts" (part superior dreta)
            > Prémer l'ítem "New tab"

    6. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Browser_C.PNG".

    7. Obrir una nova finestra

        - Es pot prémer la combinació de tecles "Ctrl + N".
        - O també es poden seguir aquests passos:

            > Prémer la icona "Tres Punts" (part superior dreta)
            > Prémer l'ítem "New window"

    8. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Browser_D.PNG".

    9. Obrir una nova finestra privada

        - Es pot prémer la combinació de tecles "Ctrl + Shift + N".
        - O també es poden seguir aquests passos:

            > Prémer la icona "Tres Punts" (part superior dreta)
            > Prémer l'ítem "New incognito window"

    10. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Browser_E.PNG".

    11. Carregar els ajustaments del navegador

        - Des de la "search bar" es pot cercar l'adreça "chrome://settings".
        - O també es poden seguir aquests passos:

            > Prémer la icona "Tres Punts" (part superior dreta)
            > Prémer l'ítem "Settings"

    12. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Browser_F.PNG".

c) Informació del Sistema

    1. Executar l'eina "Administrador de dispositivos"

        - Clicar la icona "Windows" de la barra de tasques.
        - Escriure 'disp' al camp de cerca.
        - Clicar l'ítem "Administrador de dispositivos" al llistat de resultats.

    2. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Tools_A.PNG".

    3. Executar l'eina "Información del sistema"

        - Clicar la icona "Windows" de la barra de tasques.
        - Escriure 'si' al camp de cerca.
        - Clicar l'ítem "Información del sistema" al llistat de resultats.

    4. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Tools_B.PNG".

    5. Executar l'eina "Administrador de tareas de Windows"

        - Clicar la icona "Windows" de la barra de tasques.
        - Escriure 'proce' al camp de cerca.
        - Clicar l'ítem "Usar el Administrador de tareas para ver los procesos..." al llistat de resultats.

    6. Guardar una captura de pantalla al subdirectori "GUI_Block\Screenshots\"

        - Usant l'eina "Recortes", guardar el fitxer "Tools_C.PNG".


>> Bloc CLI (Command-Line Interface) <<

a) Directoris i Fitxers

    1. Crear els directoris "CLI_Block", "dirA" i "dirB"

        md CLI_Block dirA dirB

    2. Llistar els directoris que s'acaben de crear

        dir /AD

    3. Moure els directoris "dirA" i "dirB" al directori "CLI_Block\"

        move dirA CLI_Block\
        move dirB CLI_Block\

    4. Crear els subdirectoris "CLI_Block\Documents" i "CLI_Block\Screenshots"

        md CLI_Block\Documents CLI_Block\Screenshots
    
    5. Eliminar el subdirectori "CLI_Block\Screenshots"

        rmdir CLI_Block\Screenshots

    6. Crear el subdirectori "CLI_Block\dirTemp"

        md CLI_Block\dirTemp

    7. Moure els subdirectoris "CLI_Block\dirA" i "CLI_Block\dirB" al subdirectori "CLI_Block\dirTemp\"

        cd CLI_Block\
        move dirA dirTemp\
        move dirB dirTemp\

    8. Llistar el contingut del directori "CLI_Block" (inclosos fitxers i subdirectoris)

        dir /S

    9. Ocultar el subdirectori "dirTemp\dirA"

        attrib +h dirTemp\dirA

    10. Renombrar el subdirectori "dirTemp\dirB" --> "dirC"

        move dirTemp\dirB dirTemp\dirC

    11. Llistar el contingut del subdirectori ""CLI_Block\dirTemp"

        dir dirTemp\

    12. Crear l'accés directe "CLI_Block\dC" al subdirectori "dirTemp\dirC"

        mklink /D dC dirTemp\dirC (com a usuari amb privilegis)

    13. Llistar el contingut del subdirectori "CLI_Block\dC"

        dir dC\

    14. Eliminar el subdirectori "CLI_Block\dirTemp" (inclosos fitxers i subdirectoris)

        rmdir /S dirTemp
    
    15. Eliminar l'accés directe "CLI_Block\dC"

        rmdir dC

    16. Llistar el contingut del directori "CLI_Block" (inclosos fitxers i subdirectoris)

        dir /S

    17. Crear el fitxer "CLI_Block\Documents\Bye.txt" amb el contingut "Good Bye"

        echo Good Bye > Documents\Bye.txt
    
    18. Veure el contingut del fitxer "CLI_Block\Documents\Bye.txt"

        type Documents\Bye.txt

    19. Renombrar el fitxer "CLI_Block\Documents\Bye.txt" --> "GoodBye.txt"

        cd Documents\
        move Bye.txt GoodBye.txt

    20. Canviar l'extensió del fitxer "CLI_Block\Documents\GoodBye.txt" --> "GoodBye.cc"

        move GoodBye.txt GoodBye.cc
    
    21. Crear una còpia del fitxer "CLI_Block\Documents\GoodBye.cc" --> "CLI_Block\Documents\GoodBye_orig.cc"

        copy GoodBye.cc GoodBye_orig.cc

    22. Eliminar el fitxer "CLI_Block\Documents\GoodBye.cc"

        del GoodBye.cc

    23. Crear l'accés directe "CLI_Block\Bye" al fitxer "CLI_Block\Documents\GoodBye_orig.cc"

        cd ..\
        mklink Bye Documents\GoodBye_orig.cc (com a usuari amb privilegis)

    24. Llistar el contingut del directori "CLI_Block" (inclosos fitxers i subdirectoris)

        dir /S

b) Informació del Sistema

    1. Executar l'eina "Acerca de Windows"

        winver

    2. Executar l'eina "Administrador de tareas de Windows"

        taskmgr

    3. Executar l'eina "Información del sistema"

        msinfo32

    4. Executar l'eina "Configuración del sisteman"

        msconfig


>> Bloc Final <<

a) Script

    1. Crear el fitxer "blockCli.bat" amb les següents característiques:

        > Reproduïr els passos del punt 'Directoris i Fitxers' del 'Bloc CLI'.
        > Mostrar els resultats de l'execució per pantalla.
        > Escriure per pantalla "Good Bye".
        > Demanar a l'usuari que premi una tecla per finalitzar l'execució.


        echo Bloc CLI

        md CLI_Block dirA dirB
        dir /AD

        move dirA CLI_Block\
        move dirB CLI_Block\

        md CLI_Block\Documents CLI_Block\Screenshots
        rmdir CLI_Block\Screenshots

        md CLI_Block\dirTemp
        cd CLI_Block\
        move dirA dirTemp\
        move dirB dirTemp\
        dir /S

        attrib +h dirTemp\dirA
        move dirTemp\dirB dirTemp\dirC
        dir dirTemp\

        mklink /D dC dirTemp\dirC
        dir dC\

        rmdir /S dirTemp
        rmdir dC
        dir /S

        echo Good Bye > Documents\Bye.txt
        type Documents\Bye.txt

        cd Documents\
        move Bye.txt GoodBye.txt
        move GoodBye.txt GoodBye.cc
        copy GoodBye.cc GoodBye_orig.cc
        del GoodBye.cc

        cd ..\
        mklink Bye Documents\GoodBye_orig.cc
        dir /S

        cd ..\
        echo Good Bye
        pause 

b) Últims Passos

    1. Crear el directori "Practice_NomCognom" (substituïr "NomCognom" pel nom i el primer cognom)

        - Prémer el botó dret del mouse.
        - Clicar l'ítem "Nuevo".
        - Clicar l'ítem "Carpeta".
        - Escriure "Practice_GabrielCerdà" com a nom del directori.

    2. Moure els directoris "GUI_Block" i "CLI_Block" al directori "Practice_NomCognom\"

        - Seleccionar els 2 directoris amb el mouse.
        - Amb el mouse, arrossegar els 2 directoris fins al directori "Practice_NomCognom".

    3. Moure el fitxer "blockCli.bat" al directori "Practice_NomCognom\"

        - Seleccionar el fitxer amb el mouse.
        - Amb el mouse, arrossegar el fitxer fins al directori "Practice_NomCognom".

    4. Llistar el contingut del directori "Practice_NomCognom" (inclosos fitxers i subdirectoris)

        - Obrir la terminal i executar els següents comandaments:

            cd Desktop\Practice_NomCognom\
            dir /S

    5. Comprimir el directori "Practice_NomCognom" --> "Practice_NomCognom.zip"

        - Prémer el botó dret del mouse sobre el directori "Practice_NomCognom".
        - Clicar l'ítem "Enviar a".
        - Clicar l'ítem "Carpeta comprimida (en zip)".
        - Deixar el nom del fitxer comprimit intacte.

