echo Bloc CLI

md CLI_Block dirA dirB
dir /AD

move dirA CLI_Block\
move dirB CLI_Block\

md CLI_Block\Documents CLI_Block\Screenshots
rmdir CLI_Block\Screenshots

md CLI_Block\dirTemp
cd CLI_Block\
move dirA dirTemp\
move dirB dirTemp\
dir /S

attrib +h dirTemp\dirA
move dirTemp\dirB dirTemp\dirC
dir dirTemp\

mklink /D dC dirTemp\dirC
dir dC\

rmdir /S dirTemp
rmdir dC
dir /S

echo Good Bye > Documents\Bye.txt
type Documents\Bye.txt

cd Documents\
move Bye.txt GoodBye.txt
move GoodBye.txt GoodBye.cc
copy GoodBye.cc GoodBye_orig.cc
del GoodBye.cc

cd ..\
mklink Bye Documents\GoodBye_orig.cc
dir /S

cd ..\
echo Good Bye
pause
